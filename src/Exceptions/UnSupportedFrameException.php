<?php

namespace longhu\MigrateGenerator\Exceptions;

class UnSupportedFrameException extends \Exception
{
    protected $message = 'unsupported frame migrate generator';
}
